/* --------------------- */
/* --- =Setup --- */
/* --------------------- */
var numGroundImgs = 3, // set number for different ground tile images in img/ground/ - must be names 1.jpg, 2.jpg, etc.
	groundImgDimensions = 50, // set if you want to change the dimensions squared - set to 'default to detect actual image size'
	canvasWidth = 800, // works best if imgs divide evenly
	canvasHeight = 400,
	orcWidth = 92, // if the orc image is resized, need to change it here for now (make this dynamic)
	orcHeight = 108;


/* --------------------- */
/* --- =Utility Functions --- */
/* --------------------- */
function randomNumber(min, max) {
	return Math.floor(Math.random() * (max - min + 1) + min);
}

var consoleList = document.getElementById('console-list'); // print message to the html console
function consoleLine(outputText) {
	var li = document.createElement('li');
	consoleList.insertBefore(li, consoleList.firstChild);
	li.innerHTML = outputText;
}


/* --------------------- */
/* --- =Create Canvas --- */
/* --------------------- */
var canvas = document.getElementById('canvas-1'); // main canvas
var cxt = canvas.getContext('2d');
canvas.width = canvasWidth;
canvas.height = canvasHeight;
consoleLine('canvas created');

// use secondary canvas to save the random background
var canvas2 = document.getElementById('canvas-2');
var cxt2 = canvas2.getContext('2d');
canvas2.width = canvas.width;
canvas2.height = canvas.height;


/* --------------------- */
/* --- =create randomly generated ground --- */
/* --------------------- */
var groundImgArray = [];
var groundImgReadyArray = [];
for (var i = 1; i <= numGroundImgs; i++) { // create images and set src
	groundImgArray[i] = new Image();
	groundImgArray[i].src = 'img/ground/' + i +'.jpg';
	groundImgReadyArray[i] = false;
	groundImgArray[i].onload = imgReady(i); // set event for when the image is loaded
}

function imgReady(a) {
	groundImgReadyArray[a] = true; // set to true on the image when it is loaded
}

function checkimgsReady(a) { // check to see if all images are loaded
	var sum = 0;
	for (var i = 1; i <= a; i++) {
		sum += groundImgReadyArray[i];
	}
	return sum;
}

var intervalCount = 1;
var checkImgsReadyInterval = setInterval(function(){
	checkimgsReady(numGroundImgs);
	if (checkimgsReady(numGroundImgs) == numGroundImgs) { // check to see if all images are loaded
		clearInterval(checkImgsReadyInterval);
		consoleLine('ground images loaded');
		generateGround(numGroundImgs); // start ground generating
	} else if (intervalCount == 100) { // if images don't load after 10 seconds, stop trying
		consoleLine('Something is wrong. Ground images are not loading.');
		clearInterval(checkImgsReadyInterval);
	} else {
		++intervalCount;
	}
}, 100);

function getGroundImgDimensions(groundImgDimensions) {
	if ( groundImgDimensions == 'default' ) { // if not value was set in Setup, return the actual dimension of the first image
		return groundImgArray[1].width;
	} else {
		return groundImgDimensions; // if value was set in Setup, return that value
	}
}

function generateGround() {
	var tilesRows = canvas.height / getGroundImgDimensions(groundImgDimensions);
	var tilesColumns = canvas.width / getGroundImgDimensions(groundImgDimensions);
	var x = 0;
	var y = 0;
	for (var row = 1; row <= tilesRows; row++) { // draw the images
		for (var column = 1; column <= tilesColumns; column++) {
			var currentImage = groundImgArray[randomNumber(1, numGroundImgs)]; // choose one of the images randomly
			cxt.drawImage(currentImage, x, y);
			x += getGroundImgDimensions(groundImgDimensions);
		}
		x = 0;
		y += getGroundImgDimensions(groundImgDimensions);
	}
	consoleLine('randomly generated ground complete');
	cxt2.drawImage(canvas, 0, 0); // save ground onto secondary canvas (so it doesn't get rerwriten)
}

/* --------------------- */
/* --- =Orc --- */
/* --------------------- */

var orcImage = new Image();
orcImage.onload = function () {
	cxt.drawImage(orcImage, (canvas.width / 2) - (orcWidth / 2), (canvas.height / 2) - (orcHeight / 2));
	consoleLine('orc spawned');
};
orcImage.src = "img/orc.png";

var orc = {
	x: (canvas.width / 2) - (orcWidth / 2),
	y: (canvas.height / 2) - (orcHeight / 2)
};


/* --------------------- */
/* --- =Controls --- */
/* --------------------- */
var arrowKeysImg = document.getElementById('arrow-keys-img'),
	arrowKeysMessage = document.getElementById('use-arrow-keys'),
	speedStat = document.getElementById('speed'),
	orcPositionStat = document.getElementById('orc-position'),
	redrawsStat = document.getElementById('redraws'),
	increaseSpeedButton = document.getElementById('increase-speed'),
	decreaseSpeedButton = document.getElementById('decrease-speed'),
	newBackgroundButton = document.getElementById('new-background');

orcPositionStat.innerHTML = Math.floor(orc.x) + ", " + Math.floor(orc.y);

var arrowKeysHint = function() {
	arrowKeysMessage.className = 'use-arrow-keys-message-visible'; // show hit
	setTimeout(function(){arrowKeysMessage.className = 'use-arrow-keys-message';}, 2000); // hide hint in 2 seconds
};

arrowKeysImg.addEventListener('click', arrowKeysHint);

var increaseSpeed = function() {
	if (speed < 5 ) {
		speed++;
		consoleLine('speed increased');
	}
	else {
		consoleLine('max speed');
	}
};

var decreaseSpeed = function() {
	if (speed > 1) {
		speed--;
		consoleLine('speed decreased');
	} else {
		consoleLine('min speed');
	}
};

increaseSpeedButton.addEventListener('click', increaseSpeed);
decreaseSpeedButton.addEventListener('click', decreaseSpeed);

var newBackground = function() {
	generateGround();
};

newBackgroundButton.addEventListener('click', newBackground);


var velY = 0,
    velX = 0,
    speed = 3,
    friction = 0.8,
    keys = [],
    facing = 'left';



var updateCounter = 1;
function update() {

    if (keys[38]) { // up
        if (velY > -speed) {
            velY--;
        }
    }
    
    if (keys[40]) {
        if (velY < speed) { // down
            velY++;
        }
    }
    if (keys[39]) {
        if (velX < speed) { // right
            velX++;
            facing = 'right';
        }
    }
    if (keys[37]) { // left
        if (velX > -speed) {
            velX--;
            facing = 'left';
        }
    }

    velY *= friction;
    orc.y += velY;
    velX *= friction;
    orc.x += velX;

    if (orc.x >= canvas.width - orcWidth) {
        orc.x = canvas.width - orcWidth;
    } else if (orc.x <= 5) {
        orc.x = 5;
    }

    if (orc.y > canvas.height - orcHeight) {
        orc.y = canvas.height - orcHeight;
    } else if (orc.y <= 5) {
        orc.y = 5;
    }
    cxt.clearRect(0, 0, canvas.width, canvas.height);
    cxt.drawImage(canvas2, 0, 0);
    cxt.drawImage(orcImage, orc.x, orc.y);

    speedStat.innerHTML = speed;
        if ( isMoving ) {
		orcPositionStat.innerHTML = Math.floor(orc.x) + ", " + Math.floor(orc.y);

    }
    redrawsStat.innerHTML = updateCounter;
    updateCounter++;
    setTimeout(update, 10);
}

update();

var isMoving = false;

document.body.addEventListener("keydown", function (e) {
    keys[e.keyCode] = true;
    isMoving = true;
});
document.body.addEventListener("keyup", function (e) {
    keys[e.keyCode] = false;
    isMoving = false;
});
